const actions = [
    {
        active: false,
        state: 'circle'
    },
    {
        active: true,
        state: 'line'
    },
    {
        active: true,
        state: 'square'
    },
    {
        active: true,
        state: 'point'
    },
    {
        active: false,
        state: 'arrow'
    },
    {
        active: true,
        state: 'line'
    },
    {
        active: false,
        state: 'square'
    },
    {
        active: true,
        state: 'line'
    },
    {
        active: true,
        state: 'square'
    },
    {
        active: true,
        state: 'line'
    },
    {
        active: false,
        state: 'point'
    },
    {
        active: false,
        state: 'line'
    },
    {
        active: false,
        state: 'circle'
    },
];

printFigures(actions)
console.log('количество последних отмененных действий ', calcLastCancels(actions));
console.log('количество "видимых" квадратов ', calcVisibleSquare(actions));
console.log('количество всех отмененных действий пользователем ', calcCanceledAction(actions));

//Отобразить в html только видимые фигуры ввиде кнопок с названием фигуры, 

function printFigures(arr) {
    document.body.innerHTML = '';
    arr.filter(item => item.active).forEach(item => document.body.append(createFigure(item)));
}

function createFigure(obj) {
    const btn = document.createElement('button');
    btn.innerText = obj.state;
    btn.onclick = () => {
        obj.active = !obj.active;
        printFigures(actions);
    };
    return btn;
}


// Найти количество последних отмененных действий (исходный массив не должен менятся)
function calcLastCancels(arr) {
    const canceledActionsLast = [...arr].reverse().findIndex(item => item.active);
    return canceledActionsLast;
}

//Найти количество "видимых" квадратов (square)
function calcVisibleSquare(arr) {
    const newArr = arr.filter(item => item.active && item.state === 'square');
    return newArr.length;
}


// Найти количество всех отмененных действий пользователем
function calcCanceledAction(arr) {
    const newArr = arr.filter(item => !item.active);
    return newArr.length;
}